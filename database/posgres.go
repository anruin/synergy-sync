package database

import (
	"log"
	"os"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/postgresql"
)

type PostgresDB struct {
	DB
	Settings postgresql.ConnectionURL
	Session sqlbuilder.Database
}

func (db *PostgresDB) connect(Session sqlbuilder.Database) {
	dbName := os.Getenv("SynergyPostgresDBName")
	dbHost := os.Getenv("SynergyPostgresDBHost")
	dbUser := os.Getenv("SynergyPostgresDBUser")
	dbPass := os.Getenv("SynergyPostgresDBPass")

	db.Settings = postgresql.ConnectionURL{
		Database: dbName,
		Host:     dbHost,
		User:     dbUser,
		Password: dbPass,
	}

	session, err := postgresql.Open(db.Settings)

	if err != nil {
		log.Fatalf("db.Open(): %q\n", err)
	}

	db.Session = session
}

func (db *PostgresDB) disconnect() {
	defer db.Session.Close()
}

func (db *PostgresDB) create(object interface{}) {

}

func (db *PostgresDB) update(object interface{}, ids string) {

}

func (db *PostgresDB) delete(ids string) {

}
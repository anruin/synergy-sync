package database

type DB struct {
}

func (db *DB) Connect() {}

func (db *DB) Disconnect() {}

func (db *DB) Create(object interface{}) {}

func (db *DB) Update(object interface{}, ids string) {}

func (db *DB) Delete(ids string) {}


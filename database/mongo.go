package database

import (
	"log"
	"os"
	"upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mongo"
)

type MongoDB struct {
	DB
	Settings mongo.ConnectionURL
	Session db.Database
}

func (db *MongoDB) connect(Session sqlbuilder.Database) {
	dbName := os.Getenv("SynergyPostgresDBName")
	dbHost := os.Getenv("SynergyPostgresDBHost")
	dbUser := os.Getenv("SynergyPostgresDBUser")
	dbPass := os.Getenv("SynergyPostgresDBPass")

	db.Settings = mongo.ConnectionURL{
		Database: dbName,
		Host:     dbHost,
		User:     dbUser,
		Password: dbPass,
	}

	session, err := mongo.Open(db.Settings)

	if err != nil {
		log.Fatalf("db.Open(): %q\n", err)
	}

	db.Session = session
}

func (db *MongoDB) disconnect() {
	defer db.Session.Close()
}

func (db *MongoDB) create(object interface{}) {

}

func (db *MongoDB) update(object interface{}, ids string) {

}

func (db *MongoDB) delete(ids string) {

}
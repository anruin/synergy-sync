package main

import (
	"flag"
	"github.com/nats-io/go-nats"
	"gitlab.com/anruin/synergy-sync/replication"
	"log"
	"runtime"
)

func usage() {
	log.Fatalf("Usage: nats-sub [-s server] \n")
}

func main() {
	var urls = flag.String("s", nats.DefaultURL, "The nats server URLs (separated by comma)")

	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	var s = replication.Service{}
	s.Connect(urls)

	runtime.Goexit()
}

package replication

import (
	"encoding/json"
	"fmt"
	"github.com/nats-io/go-nats"
	"log"
	"time"
)

// Cloud replication service
type Service struct {
	Connection *nats.Conn
}

// connect to the message queue
func (service *Service) Connect(urls *string) {
	var err error = nil

	// Set up connection
	service.Connection, err = nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	// Subscribe
	service.Connection.Subscribe(subjectRequestAny, service.onMessageReceived)
	service.Connection.Flush()

	if err = service.Connection.LastError(); err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on [%s]\n", subjectRequestAny)
}

// On message received
func (service *Service) onMessageReceived(message *nats.Msg) {

	fmt.Printf("onMessageReceived: %s %s\n", message.Reply, message.Sub.Subject)

	// Process message subject
	subject := string(message.Subject)

	// Decode received data
	var request = Request{}
	json.Unmarshal(message.Data, &request)

	if !checkUserPermissions(request) {
		service.replyToPublisher(message.Reply, Response{
			Status: responseStatusError,
			Message: "403 Forbidden",
		})
	}

	result := false

	switch subject {
	case subjectRequestConnect:
		result = service.onConnect(request, message.Reply)
		break
	case subjectRequestCreate:
		result = service.onCreate(request, message.Reply)
		break
	case subjectRequestUpdate:
		result = service.onUpdate(request, message.Reply)
		break
	case subjectRequestDelete:
		result = service.onDelete(request, message.Reply)
		break
	default:
		break
	}

	if !result {
		service.replyToPublisher(message.Reply, Response{
			Status: responseStatusError,
			Message: "422 Unprocessable Entity",
		})
	}
}

// Received connect request
func (service *Service) onConnect(request Request, reply string) (bool) {

	// Todo: Check if client is allowed to connect (auth)
	// Todo: Give the client all initial data

	hostInfo, err := json.Marshal(request.HostInfo)

	if err != nil {
		log.Printf("Error marshalling host info!")
		return false
	}

	data := SyncData{
		Payload: string(hostInfo),
	}

	response := Response{
		Status:  responseStatusOK,
		Message: "Connected",
		Data:    data,
	}

	// Reply to promoter
	service.publishMessage(reply, response)

	// Broadcast message on new client connection
	service.publishMessage(subjectResponseConnect, response)

	return true
}

// Received object create promotion
func (service *Service) onCreate(request Request, reply string) bool {

	if !validateObject(request.Data) {
		return false
	}

	data := SyncData{
		Id:        request.Data.Id,
		Model:     request.Data.Model,
		Payload:   request.Data.Payload,
		Timestamp: time.Now().Unix(),
	}

	response := Response{
		Status: responseStatusOK,
		Data:   data,
	}

	// Reply to promoter
	service.publishMessage(reply, response)

	// Distribute approved object creation.
	service.publishMessage(subjectResponseCreate, response)

	createObject(request.Data)

	return true
}

// Received object update promotion
func (service *Service) onUpdate(request Request, reply string) bool {

	if !validatePatch(request.Data) {
		return false
	}

	data := SyncData{
		Id:        request.Data.Id,
		Model:     request.Data.Model,
		Payload:   request.Data.Payload,
		Timestamp: time.Now().Unix(),
	}

	response := Response{
		Status: responseStatusOK,
		Data:   data,
	}

	// Reply to promoter
	service.publishMessage(reply, response)

	// Distribute approved object update.
	service.publishMessage(subjectResponseUpdate, response)

	updateObject(request.Data)

	return true
}

// Received object delete promotion
func (service *Service) onDelete(request Request, reply string) bool {

	data := SyncData{
		Id:        request.Data.Id,
		Model:     request.Data.Model,
		Payload:   request.Data.Payload,
		Timestamp: time.Now().Unix(),
	}

	response := Response{
		Status: responseStatusOK,
		Data:   data,
	}

	// Reply to promoter
	service.publishMessage(reply, response)

	// Distribute approved object deletion.
	service.publishMessage(subjectResponseDelete, response)

	deleteObject(request.Data)

	return true
}

func (service *Service) replyToPublisher(reply string, data Response) {
	service.publishMessage(reply, data)
}

// Publish message for clients
func (service *Service) publishMessage(subject string, data Response) {

	serializedData, err := json.Marshal(data)

	if err != nil {
		log.Printf("Error marshalling Response data!")
		return
	}

	service.Connection.Publish(subject, serializedData)
}

// FIXME Add real permission check
func checkUserPermissions(request Request) (bool) {
	_ = request
	return true
}

// FIXME Add real validation
func validateObject(data SyncData) (bool) {
	_ = data
	return true
}

// FIXME Add real validation
func validatePatch(data SyncData) (bool) {
	_ = data
	return true
}

func createObject(data SyncData) {
	_ = data
}

func updateObject(data SyncData) {
	_ = data
}

func deleteObject(data SyncData) {
	_ = data
}

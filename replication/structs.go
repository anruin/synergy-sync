package replication

// Helper message subject struct
type Subject struct {
	Protocol string
	Domain   string
	Channel  string
	Action   string
}

// Host information
type HostInfo struct {
	Id        string `json:"p_id"`
	UserId    string `json:"p_userId"`
	UserToken string `json:"p_userToken"`
	UpdatedAt int64  `json:"p_updatedAt"`
}

// Synchronisation data
type SyncData struct {
	Id        string `json:"p_id"`
	Model     string `json:"p_model"`
	Payload   string `json:"p_payload"`
	Timestamp int64  `json:"p_timestamp"`
}

// Client sync request
type Request struct {
	HostInfo HostInfo `json:"p_hostInfo"`
	Data     SyncData `json:"p_data"`
}

// Client sync response
type Response struct {
	Status  string   `json:"p_status"`
	Message string   `json:"p_message"`
	Data    SyncData `json:"p_data"`
}

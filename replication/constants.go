package replication

// Constants

const subjectRequestAny = "synergy.client.>"

const subjectRequestConnect = "synergy.client.sync.connect"
const subjectRequestCreate = "synergy.client.sync.create"
const subjectRequestUpdate = "synergy.client.sync.update"
const subjectRequestDelete = "synergy.client.sync.delete"

const subjectResponseConnect = "synergy.server.sync.connect"
const subjectResponseCreate = "synergy.server.sync.create"
const subjectResponseUpdate = "synergy.server.sync.update"
const subjectResponseDelete = "synergy.server.sync.delete"

const responseStatusOK = "ok"
const responseStatusError = "error"

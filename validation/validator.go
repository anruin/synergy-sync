package validation

type Validator interface {
	validate() bool
}
